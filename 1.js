(function (cjs, an) {

var p; // shortcut to reference prototypes
var lib={};var ss={};var img={};
lib.ssMetadata = [
		{name:"1_atlas_1", frames: [[170,34,31,9],[170,0,32,32],[88,82,34,4],[0,88,80,80],[0,0,86,86],[82,88,80,80],[88,0,80,80],[204,0,32,32],[164,82,69,80]]}
];


(lib.AnMovieClip = function(){
	this.currentSoundStreamInMovieclip;
	this.actionFrames = [];
	this.soundStreamDuration = new Map();
	this.streamSoundSymbolsList = [];

	this.gotoAndPlayForStreamSoundSync = function(positionOrLabel){
		cjs.MovieClip.prototype.gotoAndPlay.call(this,positionOrLabel);
	}
	this.gotoAndPlay = function(positionOrLabel){
		this.clearAllSoundStreams();
		this.startStreamSoundsForTargetedFrame(positionOrLabel);
		cjs.MovieClip.prototype.gotoAndPlay.call(this,positionOrLabel);
	}
	this.play = function(){
		this.clearAllSoundStreams();
		this.startStreamSoundsForTargetedFrame(this.currentFrame);
		cjs.MovieClip.prototype.play.call(this);
	}
	this.gotoAndStop = function(positionOrLabel){
		cjs.MovieClip.prototype.gotoAndStop.call(this,positionOrLabel);
		this.clearAllSoundStreams();
	}
	this.stop = function(){
		cjs.MovieClip.prototype.stop.call(this);
		this.clearAllSoundStreams();
	}
	this.startStreamSoundsForTargetedFrame = function(targetFrame){
		for(var index=0; index<this.streamSoundSymbolsList.length; index++){
			if(index <= targetFrame && this.streamSoundSymbolsList[index] != undefined){
				for(var i=0; i<this.streamSoundSymbolsList[index].length; i++){
					var sound = this.streamSoundSymbolsList[index][i];
					if(sound.endFrame > targetFrame){
						var targetPosition = Math.abs((((targetFrame - sound.startFrame)/lib.properties.fps) * 1000));
						var instance = playSound(sound.id);
						var remainingLoop = 0;
						if(sound.offset){
							targetPosition = targetPosition + sound.offset;
						}
						else if(sound.loop > 1){
							var loop = targetPosition /instance.duration;
							remainingLoop = Math.floor(sound.loop - loop);
							if(targetPosition == 0){ remainingLoop -= 1; }
							targetPosition = targetPosition % instance.duration;
						}
						instance.loop = remainingLoop;
						instance.position = Math.round(targetPosition);
						this.InsertIntoSoundStreamData(instance, sound.startFrame, sound.endFrame, sound.loop , sound.offset);
					}
				}
			}
		}
	}
	this.InsertIntoSoundStreamData = function(soundInstance, startIndex, endIndex, loopValue, offsetValue){ 
 		this.soundStreamDuration.set({instance:soundInstance}, {start: startIndex, end:endIndex, loop:loopValue, offset:offsetValue});
	}
	this.clearAllSoundStreams = function(){
		var keys = this.soundStreamDuration.keys();
		for(var i = 0;i<this.soundStreamDuration.size; i++){
			var key = keys.next().value;
			key.instance.stop();
		}
 		this.soundStreamDuration.clear();
		this.currentSoundStreamInMovieclip = undefined;
	}
	this.stopSoundStreams = function(currentFrame){
		if(this.soundStreamDuration.size > 0){
			var keys = this.soundStreamDuration.keys();
			for(var i = 0; i< this.soundStreamDuration.size ; i++){
				var key = keys.next().value; 
				var value = this.soundStreamDuration.get(key);
				if((value.end) == currentFrame){
					key.instance.stop();
					if(this.currentSoundStreamInMovieclip == key) { this.currentSoundStreamInMovieclip = undefined; }
					this.soundStreamDuration.delete(key);
				}
			}
		}
	}

	this.computeCurrentSoundStreamInstance = function(currentFrame){
		if(this.currentSoundStreamInMovieclip == undefined){
			if(this.soundStreamDuration.size > 0){
				var keys = this.soundStreamDuration.keys();
				var maxDuration = 0;
				for(var i=0;i<this.soundStreamDuration.size;i++){
					var key = keys.next().value;
					var value = this.soundStreamDuration.get(key);
					if(value.end > maxDuration){
						maxDuration = value.end;
						this.currentSoundStreamInMovieclip = key;
					}
				}
			}
		}
	}
	this.getDesiredFrame = function(currentFrame, calculatedDesiredFrame){
		for(var frameIndex in this.actionFrames){
			if((frameIndex > currentFrame) && (frameIndex < calculatedDesiredFrame)){
				return frameIndex;
			}
		}
		return calculatedDesiredFrame;
	}

	this.syncStreamSounds = function(){
		this.stopSoundStreams(this.currentFrame);
		this.computeCurrentSoundStreamInstance(this.currentFrame);
		if(this.currentSoundStreamInMovieclip != undefined){
			var soundInstance = this.currentSoundStreamInMovieclip.instance;
			if(soundInstance.position != 0){
				var soundValue = this.soundStreamDuration.get(this.currentSoundStreamInMovieclip);
				var soundPosition = (soundValue.offset?(soundInstance.position - soundValue.offset): soundInstance.position);
				var calculatedDesiredFrame = (soundValue.start)+((soundPosition/1000) * lib.properties.fps);
				if(soundValue.loop > 1){
					calculatedDesiredFrame +=(((((soundValue.loop - soundInstance.loop -1)*soundInstance.duration)) / 1000) * lib.properties.fps);
				}
				calculatedDesiredFrame = Math.floor(calculatedDesiredFrame);
				var deltaFrame = calculatedDesiredFrame - this.currentFrame;
				if(deltaFrame >= 2){
					this.gotoAndPlayForStreamSoundSync(this.getDesiredFrame(this.currentFrame,calculatedDesiredFrame));
				}
			}
		}
	}
}).prototype = p = new cjs.MovieClip();
// symbols:



(lib.CachedBmp_7 = function() {
	this.initialize(ss["1_atlas_1"]);
	this.gotoAndStop(0);
}).prototype = p = new cjs.Sprite();



(lib.BrickGrey = function() {
	this.initialize(ss["1_atlas_1"]);
	this.gotoAndStop(1);
}).prototype = p = new cjs.Sprite();



(lib.CachedBmp_6 = function() {
	this.initialize(ss["1_atlas_1"]);
	this.gotoAndStop(2);
}).prototype = p = new cjs.Sprite();



(lib.CachedBmp_5 = function() {
	this.initialize(ss["1_atlas_1"]);
	this.gotoAndStop(3);
}).prototype = p = new cjs.Sprite();



(lib.CachedBmp_1 = function() {
	this.initialize(ss["1_atlas_1"]);
	this.gotoAndStop(4);
}).prototype = p = new cjs.Sprite();



(lib.CachedBmp_3 = function() {
	this.initialize(ss["1_atlas_1"]);
	this.gotoAndStop(5);
}).prototype = p = new cjs.Sprite();



(lib.CachedBmp_4 = function() {
	this.initialize(ss["1_atlas_1"]);
	this.gotoAndStop(6);
}).prototype = p = new cjs.Sprite();



(lib.GrassCenter = function() {
	this.initialize(ss["1_atlas_1"]);
	this.gotoAndStop(7);
}).prototype = p = new cjs.Sprite();



(lib.CachedBmp_2 = function() {
	this.initialize(ss["1_atlas_1"]);
	this.gotoAndStop(8);
}).prototype = p = new cjs.Sprite();



(lib.Bullet = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_0 = function() {
		this.stop();
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).call(this.frame_0).wait(2));

	// Layer_1
	this.instance = new lib.CachedBmp_6();
	this.instance.setTransform(-0.95,-1,0.5,0.5);

	this.instance_1 = new lib.CachedBmp_7();
	this.instance_1.setTransform(-0.3,-2.2,0.5,0.5);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance}]}).to({state:[{t:this.instance_1}]},1).wait(1));

	this._renderFirstFrame();

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-0.9,-2.2,17,4.5);


(lib.Tile = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_0 = function() {
		this.stop();
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).call(this.frame_0).wait(2));

	// Layer_1
	this.instance = new lib.CachedBmp_4();
	this.instance.setTransform(0,0,0.5,0.5);

	this.instance_1 = new lib.CachedBmp_5();
	this.instance_1.setTransform(0,0,0.5,0.5);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance}]}).to({state:[{t:this.instance_1}]},1).wait(1));

	this._renderFirstFrame();

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,40,40);


(lib.Pacman = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.CachedBmp_2();
	this.instance.setTransform(-20.5,-20.5,0.5,0.5);

	this.instance_1 = new lib.CachedBmp_3();
	this.instance_1.setTransform(-20.5,-20.5,0.5,0.5);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance}]}).to({state:[{t:this.instance_1}]},14).wait(16));

	this._renderFirstFrame();

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-20.5,-20.5,40,40);


(lib.AbilityBar = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_0 = function() {
		this.stop();
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).call(this.frame_0).wait(5));

	// Border
	this.instance = new lib.CachedBmp_1();
	this.instance.setTransform(-1.5,-1.5,0.5,0.5);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(5));

	this._renderFirstFrame();

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-1.5,-1.5,43,43);


// stage content:
(lib._1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	this.actionFrames = [0];
	this.isSingleFrame = false;
	// timeline functions:
	this.frame_0 = function() {
		if(this.isSingleFrame) {
			return;
		}
		if(this.totalFrames == 1) {
			this.isSingleFrame = true;
		}
		this.clearAllSoundStreams();
		 
		function Create2DArray(rows) {
		  var arr = [];
		
		  for (var i=0;i<rows;i++) 
		  {
		     arr[i] = [];
		  }
		  return arr;
		}
		
		var gcd = function(a, b) {
		  if (!b) {
		    return a;
		  }
		
		  return gcd(b, a % b);
		}
		
		var stage_width = stage.canvas.width;
		var stage_height = stage.canvas.height;
		
		var tile_size = 40;
		var display_tile_limit_column = stage_width / tile_size;
		var display_tile_limit_row = stage_height / tile_size;
		var tiles_display = Create2DArray(stage_height / tile_size + 1);
		var tile_count_row, tile_count_column;
		tile_count_row = 22;
		tile_count_column = 22;
		var tiles = Create2DArray(tile_count_row);
		
		for(var row=0; row < tile_count_row; row++)
		{
		  for(var column=0; column < tile_count_column; column++)	
		  {
				if(row == 0 || column == 0 || row == tile_count_row - 1 || column == tile_count_column - 1)
				{
					tiles[row][column] = 1;
				}
				else if((row % 2) == 0 && (column % 2) == 0)
				{
					tiles[row][column] = 1;
				}
				else if(Math.random() < 0.01)
				{
					tiles[row][column] = 1;
				}
		    else
		    {
		      tiles[row][column] = 0;
		    }
		  }
		}
		
		
		//----------------
		// Player Setup
		//----------------
		var player =
		{
		  x: 0,
		  y: 0,
		  nominalBounds: undefined,
		  sprite: new lib.Pacman(),
			next_frame_to_fire: 0
		};
		
		player.x = player.sprite.nominalBounds.width / 2 + tile_size;
		player.y = player.sprite.nominalBounds.height / 2 + tile_size;
		player.nominalBounds = player.sprite.nominalBounds;
		player.sprite.stop();
		player.sprite.x = stage_width / 2;
		player.sprite.y = stage_height / 2;
		this.addChild(player.sprite);
		
		var player_stage_diff_x = -player.x;
		var player_stage_diff_y = -player.y;
		
		var scene = this;
		//----------------
		//Scene Tile Setup
		//----------------
		var tile_container = new createjs.Container();
		this.addChildAt(tile_container,0);
		
		for(var row = 0; row < display_tile_limit_row + 1; row++)
		{
		  for(var column = 0; column < display_tile_limit_column + 1; column++)
		  {
				var tile = new lib.Tile();
		    tile.addEventListener("click", onClick, false);
		/*    if(i != -1 && i != stage_width / tile_size &&
		      k != -1 && k != stage_height / tile_size)
		    {
			    tile.gotoAndStop(tiles[i][k]);
		    }
		    else
		      tile.gotoAndStop(2);*/
				
				//tile.width = tile_size;
				//tile.heigth = tile_size;
				tile.x = column * tile_size;
				tile.y = row * tile_size;
				tiles_display[row][column] = tile;
		    tile_container.addChild(tile);
		  }
		}
		
		//---------------
		//Bullet Setup
		//---------------
		function Bullet()
		{
		  this.x = 0;
		  this.y = 0;
		  this.sprite = new lib.Bullet();
		}
		var bullets = new buckets.LinkedList();
		var bullet_container = new createjs.Container();
		this.addChild(bullet_container);
		
		var enemy = new lib.Pacman();
		enemy.x = 820;
		enemy.y = 60;
		bullet_container.addChild(enemy);
		
		var grid = new PF.Grid(tiles);
		
		
		
		function onClick(event)
		{
			console.log(event.currentTarget.currentFrame);
		  console.log(event.currentTarget.x);
		}
		
		tile_container.x = stage_width / 2 + player.nominalBounds.width / 2;
		tile_container.y = stage_height / 2 + player.nominalBounds.height / 2;
		
		var direction_keys = new buckets.LinkedList();
		var dup_direction_keys = new buckets.LinkedList();
		var fire_key = false;
		
		document.addEventListener("keydown", onKeyDown, false);
		document.addEventListener("keyup", onKeyUp, false);
		stage.tickOnUpdate = false;
		stage.addEventListener("drawstart", update, false);
		
		function onKeyUp(event)
		{
			var pressed_key = String.fromCharCode(event.keyCode);
		
			if(direction_keys.remove(pressed_key))
			  player.sprite.stop();
			dup_direction_keys.remove(pressed_key);
		  switch(pressed_key)
			{
		      case "W":
		      {
					  if(direction_keys.contains("S") && !dup_direction_keys.contains("S"))
					    dup_direction_keys.add("S");
		        break;
					}
					case "S":
					{
						if(direction_keys.contains("W") && !dup_direction_keys.contains("W"))
					    dup_direction_keys.add("W");
						break;
					}
					case "A":
					{
						if(direction_keys.contains("D") && !dup_direction_keys.contains("D"))
					    dup_direction_keys.add("D");
						break;
					}
					case "D":
					{
						if(direction_keys.contains("A") && !dup_direction_keys.contains("A"))
					    dup_direction_keys.add("A");
						break;
					}
					case " ":
		      {
					  fire_key = false;
		      }
		  }
		}
		function onKeyDown(event)
		{
		  var pressed_key = String.fromCharCode(event.keyCode);
			if(!direction_keys.contains(pressed_key))
		  {
			  switch(pressed_key)
		    {
		      case "W":
		      {
					  dup_direction_keys.remove("S");
					  direction_keys.add(pressed_key);
		        dup_direction_keys.add(pressed_key);
		        break;
					}
					case "S":
					{
						dup_direction_keys.remove("W");
						direction_keys.add(pressed_key);
						dup_direction_keys.add(pressed_key);
						break;
					}
					case "A":
					{
						dup_direction_keys.remove("D");
						direction_keys.add(pressed_key);
						dup_direction_keys.add(pressed_key);
						break;
					}
					case "D":
					{
						dup_direction_keys.remove("A");
						direction_keys.add(pressed_key);
						dup_direction_keys.add(pressed_key);
						break;
					}
				}
			}
			if(pressed_key == " ")
		  {
			  fire_key = true;
		  }
		}
		
		function update()
		{
			var move_left = 5;
		  var has_moved = true;
		  var displacement = 0;
		  while(dup_direction_keys.size() > 0 && move_left > 0)
		  {
			  var left_x = player.x - player.nominalBounds.width / 2;
			  var left_column = Math.floor(left_x / tile_size);
			  var right_x = left_x + player.nominalBounds.width;
			  var right_column = right_x % tile_size == 0 
			    ? left_column : Math.floor(right_x / tile_size);
		    
			  var up_y = player.y - player.nominalBounds.height / 2;
		    var up_row = Math.floor(up_y / tile_size);
			  var down_y = up_y + player.nominalBounds.height;
			  var down_row = down_y % tile_size == 0
			    ? up_row : Math.floor(down_y / tile_size);
		
			  var hit_1, hit_2;
			  hit_1 = hit_2 = Number.MAX_VALUE;
			  
			  if (dup_direction_keys.last() == "W")
			  {
				  var row = up_row;
			    var dest_row = Math.floor((up_y - move_left) / tile_size);
			    for(; row > dest_row; row--)
			    {
				    if(tiles[row - 1][left_column] == 1)
				    {
						  hit_1 = row;
		          if(tiles[row - 1][right_column] == 1)
					      hit_2 = row;
		          break;	
		        }
						else if(tiles[row - 1][right_column] == 1)
		        {
						  hit_2 = row;
		          break; 
						}
			    }
			    if(row != dest_row)
			    {
						if(dup_direction_keys.size() == 1)
		        {
							if(hit_1 < hit_2 && left_x % tile_size > tile_size * 0.20)
							{
								displacement = Math.min(tile_size - left_x % tile_size, move_left);
								player.x += displacement;
		            player.sprite.rotation = 0;
								move_left -= displacement;
		            continue;
							}
							else if(hit_1 > hit_2 && right_x % tile_size < tile_size * 0.70)
							{
								displacement = Math.min(left_x % tile_size, move_left);
								player.x -= displacement;
								player.sprite.rotation = 180;
								move_left -= displacement;
		            continue;
							}
		        }
						displacement = Math.min(up_y - Math.min(hit_1,hit_2) * tile_size, move_left);
						player.y -= displacement;
						player.sprite.rotation = 270;
						move_left -= displacement;
			    }
			    else
			    {
		        if(dup_direction_keys.size() == 1)
		        {
			        player.y -= move_left;
		          move_left = 0;
		        }
						else
						{
						  displacement = Math.min(up_y % tile_size, move_left);
		          if(displacement == 0 && tiles[up_row - 1][right_column] != 1)
		          {
						    displacement = move_left % tile_size;
						  }
		          player.y -= displacement;
				      move_left -= displacement;
						}
						player.sprite.rotation = 270;
					}
			  }
				else if (dup_direction_keys.last() == "S")
			  {	
			    var dest_row = Math.floor((down_y + move_left) / tile_size);
				  var row = down_row;
			    for(; row < dest_row; row++)
			    {
				    if(tiles[row + 1][left_column] == 1)
		        {
		          hit_1 = row + 1;
		          if(tiles[row + 1][right_column] == 1)
					      hit_2 = row + 1;
		          break;
						}
				    if(tiles[row + 1][right_column] == 1)
						{
		          hit_2 = row + 1;
							break;
						}
			    }
			    if(row != dest_row)
					{
						if(dup_direction_keys.size() == 1)
		        {
				      if(hit_1 < hit_2 && left_x % tile_size > tile_size * 0.20)
							{
								displacement = Math.min(tile_size - left_x % tile_size, move_left);
								player.x += displacement;
								player.sprite.rotation = 0;
								move_left -= displacement;
		            continue;
							}
							else if(hit_1 > hit_2 && right_x % tile_size < tile_size * 0.70)
							{
								displacement = Math.min(left_x % tile_size, move_left);
								player.x -= displacement;
								player.sprite.rotation = 180;
								move_left -= displacement;
		            continue;
							}
						}
						displacement = Math.min(Math.min(hit_1,hit_2) * tile_size - down_y, move_left);
						player.y += displacement;
						player.sprite.rotation = 90;
						move_left -= displacement;
			    }
			    else
			    {
						player.sprite.rotation = 90;
					  if(dup_direction_keys.size() == 1)
		        {
			        player.y += move_left;
		          move_left = 0;
		        }
		        else
		        {
		          displacement = Math.min(tile_size - down_y % tile_size, move_left);
		          player.y += displacement;
				      move_left -= displacement;
		        }
			    }
			  }
			  else if (dup_direction_keys.last() == "D")
			  {
			    var dest_column = Math.floor((right_x + move_left) / tile_size);
		
		      var column = right_column;
			    for(; column < dest_column; column++)
			    {
				    if(tiles[up_row][column + 1] == 1)
						{
		          hit_1 = column + 1;
		          if(tiles[down_row][column + 1] == 1)
							  hit_2 = column + 1;
		          break;
		        }
					  if(tiles[down_row][column + 1] == 1)
		        {
		          hit_2 = column + 1;
							break;
		        }
					}
			    if(column != dest_column)
			    {
						if(dup_direction_keys.size() == 1)
		        {
							if(hit_1 < hit_2 && up_y % tile_size > tile_size * 0.20)
							{
								displacement = Math.min(tile_size - up_y % tile_size, move_left);
								player.y += displacement;
								player.sprite.rotation = 90;
								move_left -= displacement;
		            continue;
							}
							else if(hit_1 > hit_2 && down_y % tile_size < tile_size * 0.70)
							{
								displacement = Math.min(up_y % tile_size, move_left);
								player.y -= displacement;
								player.sprite.rotation = 270;
								move_left -= displacement;
								continue;
							}
		        }
						displacement = Math.min(Math.min(hit_1,hit_2) * tile_size - right_x, move_left);
						player.x += displacement;
						player.sprite.rotation = 0;
						move_left -= displacement;
			    }
			    else
			    {
						player.sprite.rotation = 0;
		        if(dup_direction_keys.size() == 1)
		        {
			        player.x += move_left;
		          move_left = 0;
		        }
				    else
		        {
		          displacement = Math.min(tile_size - right_x % tile_size, move_left);
		          player.x += displacement;
				      move_left -= displacement;
		        }
			    }
			  }
			  else if (dup_direction_keys.last() == "A")
			  {	
			    var dest_column = Math.floor((left_x - move_left) / tile_size);
				  var column = left_column;
			    for(; column > dest_column; column--)
			    {
				    if(tiles[up_row][column - 1] == 1)
		        {
						  hit_1 = column;
				      if(tiles[down_row][column - 1] == 1)
							  hit_2 = column;
							break;
		        }
				    if(tiles[down_row][column - 1] == 1)
						{
							hit_2 = column;
		          break;
						}
			    }
			    if(column != dest_column)
			    {
						if(dup_direction_keys.size() == 1)
		        {
				      if(hit_1 < hit_2 && up_y % tile_size > tile_size * 0.20)
				      {
								displacement = Math.min(tile_size - up_y % tile_size, move_left);
								player.y += displacement;
								player.sprite.rotation = 90;
								move_left -= displacement;
		            continue;
							}
							else if(hit_1 > hit_2 && down_y % tile_size < tile_size * 0.70)
							{
								displacement = Math.min(up_y % tile_size, move_left);
								player.y -= displacement;
								player.sprite.rotation = 270;
								move_left -= displacement;
		            continue;
							}
		        }
						displacement = Math.min(left_x - Math.min(hit_1,hit_2) * tile_size, move_left);
						player.x -= displacement;
						player.sprite.rotation = 180;
						move_left -= displacement;
					}
			    else
					{
						player.sprite.rotation = 180;
						if(dup_direction_keys.size() == 1)
		        {
			        player.x -= move_left;
		          move_left = 0;
		        }
				    else
		        {
		          displacement = Math.min(left_x % tile_size, move_left);
		          if(displacement == 0 && tiles[up_row][left_column - 1] != 1)
		          {
						    displacement = move_left % tile_size;
						  }
		          player.x -= displacement;
				      move_left -= displacement;
		        }
					}
				}
				if(displacement == 0)
				{
					if(has_moved == false)
		      {
		        if(dup_direction_keys.last() != direction_keys.last())
		          dup_direction_keys.reverse();
						break;
		      }
					has_moved = false;
				}
				else
					has_moved = true;
				dup_direction_keys.reverse();
		  }
		
		  if(fire_key && player.next_frame_to_fire <= 0)
		  {
		    var bullet = new Bullet();
		    bullet.sprite.x = player.x;
		    bullet.sprite.y = player.y;
		    bullet.sprite.rotation = player.sprite.rotation;
		    bullet.sprite.gotoAndStop(1);
		    bullet.x = player.x;
		    bullet.y = player.y;
		    bullet_container.addChild(bullet.sprite);
		    bullets.add(bullet);
		
		    player.next_frame_to_fire = 30;
		  }
		  else
		    player.next_frame_to_fire -= 1;
		
		  var bullet_speed = 15;
		  for(var bullet_index = 0; bullet_index < bullets.size(); bullet_index++)
		  {
		    var bullet = bullets.elementAtIndex(bullet_index);
		    var bullet_column = Math.floor(bullet.x / tile_size);
		    var bullet_row = Math.floor(bullet.y / tile_size);
		
		    var bullet_removed = false;
		    switch(bullet.sprite.rotation)
		    {
		      case 0:
		      {
		        var dest_column = Math.floor((bullet.x + bullet_speed) / tile_size);
		        for(var column = bullet_column; dest_column > column; column++)
		        {
						  if(tiles[bullet_row][column + 1] == 1)
		          {
		            bullet_container.removeChild(bullet.sprite);
		            bullets.removeElementAtIndex(bullet_index);
		            bullet_index -= 1;
		            bullet_removed = true;
							  break;
		          }
		        }
		        if(!bullet_removed)
		        {
		          bullet.sprite.x += bullet_speed;
		          bullet.x += bullet_speed;
						  break;
		        }
					}
		      case 90:
		      {
		        var dest_row = Math.floor((bullet.y + bullet_speed) / tile_size);
		        for(var row = bullet_row; dest_row > row; row++)
		        {
						  if(tiles[row + 1][bullet_column] == 1)
		          {
		            bullet_container.removeChild(bullet.sprite);
		            bullets.removeElementAtIndex(bullet_index);
		            bullet_index -= 1;
		            bullet_removed = true;
							  break;
		          }
		        }
		        if(!bullet_removed)
		        {
		          bullet.sprite.y += bullet_speed;
						  bullet.y += bullet_speed;
						  break;
		        }
					}
		      case 180:
		      {
		        var dest_column = Math.floor((bullet.x - bullet_speed) / tile_size);
		        for(var column = bullet_column; dest_column < column; column--)
		        {
						  if(tiles[bullet_row][column - 1] == 1)
		          {
		            bullet_container.removeChild(bullet.sprite);
		            bullets.removeElementAtIndex(bullet_index);
		            bullet_index -= 1;
		            bullet_removed = true;
							  break;
		          }
		        }
		        if(!bullet_removed)
		        {
		          bullet.sprite.x -= bullet_speed;
						  bullet.x -= bullet_speed;
						  break;
		        }
					}
		      case 270:
		      {
		        var dest_row = Math.floor((bullet.y - bullet_speed) / tile_size);
		        for(var row = bullet_row; dest_row < row; row--)
		        {
						  if(tiles[row - 1][bullet_column] == 1)
		          {
		            bullet_container.removeChild(bullet.sprite);
		            bullets.removeElementAtIndex(bullet_index);
		            bullet_index -= 1;
		            bullet_removed = true;
							  break;
		          }
		        }
		        if(!bullet_removed)
		        {
		          bullet.sprite.y -= bullet_speed;
						  bullet.y -= bullet_speed;
						  break;
		        }
					}
		    }
		  }
		
		  bullet_container.x = stage_width / 2 - player.x;
		  bullet_container.y = stage_height / 2 - player.y;
		
			var player_row = Math.floor(player.y / tile_size);
		  var player_column = Math.floor(player.x / tile_size);
		
		  if(stage_width / 2 >= player.x)
		    tile_container.x = stage_width / 2 - player.x;
			else if(player.x >= tile_size * tile_count_column - stage_width / 2)
		    tile_container.x = (tile_size * tile_count_column - stage_width / 2	 - player.x) - tile_size;
			else
		    tile_container.x = -player.x % tile_size;
			if(stage_height / 2 >= player.y)
		    tile_container.y = stage_height / 2 - player.y;
			else if(player.y >= tile_size * tile_count_row - stage_height / 2)
		    tile_container.y = (tile_size * tile_count_row - stage_height / 2	 - player.y) - tile_size;
			else
		    tile_container.y = -player.y % tile_size;
		
		  if(direction_keys.size() > 0)
		  {
				var offset_row, offset_column;
				if(player.x < stage_width / 2 + tile_size)
					offset_column = 0;
				else if(player.x >= tile_count_column * tile_size - stage_width / 2)
		      offset_column = tile_count_column - (display_tile_limit_column + 1);
				else 
					offset_column = player_column - display_tile_limit_column / 2;
		
				if(player.y < stage_height / 2 + tile_size)
					offset_row = 0;
				else if(player.y >= tile_count_row * tile_size - stage_height / 2)
					offset_row = tile_count_row - (display_tile_limit_row + 1);
				else
					offset_row = player_row - display_tile_limit_row / 2;
				
				for(var row=0; row < display_tile_limit_row + 1; row++)
				{
					for(var column=0; column < display_tile_limit_column + 1; column++)
					{
						tiles_display[row][column].gotoAndStop(tiles[row + offset_row][column + offset_column]);
					}
				}
			}
		
		      var enemy_move_left = 4;
		    var finder = new PF.AStarFinder();
		    
		var path = finder.findPath(Math.floor(enemy.x / tile_size), Math.floor(enemy.y / tile_size),
		 Math.floor(player.x / tile_size), Math.floor(player.y / tile_size), grid.clone());
		 path.push([Math.floor(player.x / tile_size), Math.floor(player.y / tile_size)]);
		  path.shift();
		var tick = 0;
		
		
		    while(enemy_move_left != 0 && tick != path.length)
		    {
					var x_displacement = (path[tick])[0] * tile_size - (enemy.x - enemy.nominalBounds.width / 2);
		      var y_displacement = (path[tick])[1] * tile_size - (enemy.y - enemy.nominalBounds.height / 2);
		      if(x_displacement == 0 && y_displacement == 0)
		        break;
		      if(y_displacement == 0 || Math.abs(x_displacement) < Math.abs(y_displacement))
					{
						if(Math.abs(x_displacement) < enemy_move_left)
						{
							enemy.x += x_displacement;
							enemy_move_left -= Math.abs(x_displacement);
							tick += 1;
						}
						else
						{
							enemy.x += x_displacement < 0 ? enemy_move_left * -1 : enemy_move_left;
							enemy_move_left = 0;
						}
					}
					if(y_displacement != 0)
					{
						if(Math.abs(y_displacement) < enemy_move_left)
						{
							enemy.y += y_displacement;
							enemy_move_left -= Math.abs(y_displacement);
							tick += 1;
						}
						else
						{
							enemy.y += y_displacement < 0 ? enemy_move_left * -1 : enemy_move_left;
							enemy_move_left = 0;
						}
		      }
		    }
		}
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).call(this.frame_0).wait(1));

	this._renderFirstFrame();

}).prototype = p = new lib.AnMovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,0,0);
// library properties:
lib.properties = {
	id: '4599C9361BCECC43A1CDCC4657DF17A4',
	width: 640,
	height: 480,
	fps: 60,
	color: "#FFFFFF",
	opacity: 1.00,
	manifest: [
		{src:"images/1_atlas_1.png?1590891902264", id:"1_atlas_1"}
	],
	preloads: []
};



// bootstrap callback support:

(lib.Stage = function(canvas) {
	createjs.Stage.call(this, canvas);
}).prototype = p = new createjs.Stage();

p.setAutoPlay = function(autoPlay) {
	this.tickEnabled = autoPlay;
}
p.play = function() { this.tickEnabled = true; this.getChildAt(0).gotoAndPlay(this.getTimelinePosition()) }
p.stop = function(ms) { if(ms) this.seek(ms); this.tickEnabled = false; }
p.seek = function(ms) { this.tickEnabled = true; this.getChildAt(0).gotoAndStop(lib.properties.fps * ms / 1000); }
p.getDuration = function() { return this.getChildAt(0).totalFrames / lib.properties.fps * 1000; }

p.getTimelinePosition = function() { return this.getChildAt(0).currentFrame / lib.properties.fps * 1000; }

an.bootcompsLoaded = an.bootcompsLoaded || [];
if(!an.bootstrapListeners) {
	an.bootstrapListeners=[];
}

an.bootstrapCallback=function(fnCallback) {
	an.bootstrapListeners.push(fnCallback);
	if(an.bootcompsLoaded.length > 0) {
		for(var i=0; i<an.bootcompsLoaded.length; ++i) {
			fnCallback(an.bootcompsLoaded[i]);
		}
	}
};

an.compositions = an.compositions || {};
an.compositions['4599C9361BCECC43A1CDCC4657DF17A4'] = {
	getStage: function() { return exportRoot.stage; },
	getLibrary: function() { return lib; },
	getSpriteSheet: function() { return ss; },
	getImages: function() { return img; }
};

an.compositionLoaded = function(id) {
	an.bootcompsLoaded.push(id);
	for(var j=0; j<an.bootstrapListeners.length; j++) {
		an.bootstrapListeners[j](id);
	}
}

an.getComposition = function(id) {
	return an.compositions[id];
}


an.makeResponsive = function(isResp, respDim, isScale, scaleType, domContainers) {		
	var lastW, lastH, lastS=1;		
	window.addEventListener('resize', resizeCanvas);		
	resizeCanvas();		
	function resizeCanvas() {			
		var w = lib.properties.width, h = lib.properties.height;			
		var iw = window.innerWidth, ih=window.innerHeight;			
		var pRatio = window.devicePixelRatio || 1, xRatio=iw/w, yRatio=ih/h, sRatio=1;			
		if(isResp) {                
			if((respDim=='width'&&lastW==iw) || (respDim=='height'&&lastH==ih)) {                    
				sRatio = lastS;                
			}				
			else if(!isScale) {					
				if(iw<w || ih<h)						
					sRatio = Math.min(xRatio, yRatio);				
			}				
			else if(scaleType==1) {					
				sRatio = Math.min(xRatio, yRatio);				
			}				
			else if(scaleType==2) {					
				sRatio = Math.max(xRatio, yRatio);				
			}			
		}			
		domContainers[0].width = w * pRatio * sRatio;			
		domContainers[0].height = h * pRatio * sRatio;			
		domContainers.forEach(function(container) {				
			container.style.width = w * sRatio + 'px';				
			container.style.height = h * sRatio + 'px';			
		});			
		stage.scaleX = pRatio*sRatio;			
		stage.scaleY = pRatio*sRatio;			
		lastW = iw; lastH = ih; lastS = sRatio;            
		stage.tickOnUpdate = false;            
		stage.update();            
		stage.tickOnUpdate = true;		
	}
}
an.handleSoundStreamOnTick = function(event) {
	if(!event.paused){
		var stageChild = stage.getChildAt(0);
		if(!stageChild.paused){
			stageChild.syncStreamSounds();
		}
	}
}


})(createjs = createjs||{}, AdobeAn = AdobeAn||{});
var createjs, AdobeAn;